package Controlador;

import Modelo.dbCotizacion;
import Modelo.Cotizacion;
import Vista.dlgCotizacion;

import java.sql.Date;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Controlador implements ActionListener {

    dbCotizacion db;
    Cotizacion coti;
    dlgCotizacion vista;
    
    public Controlador(dbCotizacion db,Cotizacion coti, dlgCotizacion vista){
        this.db = db;
        this.coti = coti;
        this.vista = vista;
        
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
        
    }
    
    private void iniciarVista() throws Exception{
        vista.setTitle(":: COTIZACIONES ::");
        vista.setSize(620, 615);
        vista.setVisible(true);
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            vista.isInsertar = true;
            vista.txtNumCotizacion.setEnabled(true);
            vista.txtdescripcion.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.spnPorcentaje.setEnabled(true);
            vista.btnBuscar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnBorrar.setEnabled(true);
            vista.rdb12.setEnabled(true);
            vista.rdb24.setEnabled(true);
            vista.rdb36.setEnabled(true);
            vista.rdb48.setEnabled(true);
            vista.rdb60.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
        }
        if (e.getSource() == vista.btnCerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        if (e.getSource() == vista.btnLimpiar) {
            vista.txtNumCotizacion.setText("");
            vista.txtdescripcion.setText("");
            vista.txtPrecio.setText("");
            vista.spnPorcentaje.setValue(10);
            vista.rdb12.setSelected(true);
            vista.lblPagoInicial.setText("");
            vista.lblPagoMensual.setText("");
            vista.lblTotalFin.setText("");
        }
        if (e.getSource() == vista.btnCancelar) {
            vista.limpiar();
            vista.deshabilitar();
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            
        }
        if (e.getSource() == vista.btnGuardar) {
            dbCotizacion db = new dbCotizacion();
            Cotizacion cot = new Cotizacion();

            cot.setNumCotizacion(vista.txtNumCotizacion.getText());
            cot.setDescripcionAutomovil(vista.txtdescripcion.getText());
            cot.setPorcentajePago(Integer.parseInt(vista.spnPorcentaje.getValue().toString()));
            cot.setPrecioAutomovil(Integer.parseInt(vista.txtPrecio.getText()));

            if(vista.rdb12.isSelected())cot.setPlazo(12);
            if(vista.rdb24.isSelected())cot.setPlazo(24);
            if(vista.rdb36.isSelected())cot.setPlazo(36);
            if(vista.rdb48.isSelected())cot.setPlazo(48);
            if(vista.rdb60.isSelected())cot.setPlazo(60);

            if(vista.isInsertar == true){
                db.insertar(cot);
                JOptionPane.showMessageDialog(vista, "Se agrego con exito");
                vista.limpiar();
                vista.deshabilitar();
            }
            else{
                db.actualizar(cot);
                JOptionPane.showMessageDialog(vista, "Se actualizo con exito");
                vista.limpiar();
                vista.deshabilitar();
            }
            vista.cargarListaCotizacion();
        }
        if (e.getSource() == vista.btnBuscar) {
            Cotizacion cot = new Cotizacion();
            dbCotizacion db = new dbCotizacion();
            cot.setNumCotizacion(vista.txtNumCotizacion.getText());

            cot = db.consultar(cot);
            if(cot.getIdCotizacion()==-1){
                JOptionPane.showMessageDialog(vista, "No existe");
                vista.limpiar();
                vista.deshabilitar();
            }
            else{
                vista.txtdescripcion.setText(cot.getDescripcionAutomovil());
                vista.txtPrecio.setText(String.valueOf(cot.getPrecioAutomovil()));

                switch(cot.getPlazo()){
                    case 12: vista.rdb12. setSelected (true); break;
                    case 24: vista.rdb24. setSelected (true); break;
                    case 36: vista.rdb36.setSelected (true) ;break;
                    case 48: vista.rdb48.setSelected (true) ;break;
                    case 60: vista. rdb60. setSelected (true); break;
                }
                vista.habilitar();
                vista.btnBorrar.setEnabled(true);
                vista.rdb12.setEnabled(true);
                vista.rdb24.setEnabled(true);
                vista.rdb36.setEnabled(true);
                vista.rdb48.setEnabled(true);
                vista.rdb60.setEnabled(true);
                vista.spnPorcentaje.setValue(cot.getPorcentajePago());
                vista.lblPagoInicial.setText(String.valueOf(cot.calcularPagoInicial()));
                vista.lblTotalFin.setText(String.valueOf(cot.calcularTotalAfinanciar()));
                vista.lblPagoMensual.setText(String.valueOf(cot.calcularPagoMensual()));
                vista.isInsertar=false;
            }
        }
        if(e.getSource() == vista.btnBorrar){
            Cotizacion cot = new Cotizacion();
        dbCotizacion db = new dbCotizacion();
        int opcion = 0;
        
        cot.setNumCotizacion(vista.txtNumCotizacion.getText());
        opcion = JOptionPane.showConfirmDialog(vista, "¿Desea borrar el registro?", "Cotizacion",
                JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        if(opcion == JOptionPane.YES_OPTION){
            db.borrar(cot);
            JOptionPane.showMessageDialog(vista, "Se borro el registro con exito");
            vista.limpiar();
            vista.deshabilitar();
        }
        vista.cargarListaCotizacion();
        }
    }
    
    public static void main(String[] args) throws Exception {
        Cotizacion coti = new Cotizacion();
        dbCotizacion db = new dbCotizacion();
        dlgCotizacion vista = new dlgCotizacion();
        Controlador contra = new Controlador(db, coti, vista);
        contra.iniciarVista();
    }
    
}
