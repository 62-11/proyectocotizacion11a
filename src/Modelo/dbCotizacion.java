package Modelo;

import java.net.URL;
import java.sql.*;

public class dbCotizacion {
    private String MYSQLDRIVER = "com.mysql.jdbc.Driver"; 
    private String MYSQLDB;
    private Connection conexion;
    private String user = "admin";
    private String password = "ramon2008";
    private String sqlConsulta;
    private ResultSet registros;
    
        
    public dbCotizacion(){
        this.user = "admin";
        this.password = "ramon2008";
        this.MYSQLDB = "jdbc:mysql://microinformaticamx.cmwe0ss4z5wt.us-east-2.rds.amazonaws.com/sistema?user=admin&password=ramon2008";
    }
    
    public void conectar(){
        try{
            conexion = DriverManager.getConnection(MYSQLDB);
        }
        catch(SQLException e){
            System.out.println("Error al conectarse "+ e.getMessage());
        }
    }
    
    public void desconectar(){
        try{
        conexion.close();
        }
        catch(SQLException e){
                System.out.println("Error al desconectar "+ e.getMessage());
        }
    }
    
    public void insertar(Cotizacion obj){
        conectar();
        try{
            sqlConsulta = "INSERT INTO " 
                    + "cotizaciones(numCotizacion,descripcion,precio,porcentaje,plazos)" 
                    + " VALUES (?,?,?,?,?)";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            sqlComando.setString(1, obj.getNumCotizacion());
            sqlComando.setString(2, obj.getDescripcionAutomovil());
            sqlComando.setInt(3, obj.getPrecioAutomovil());
            sqlComando.setInt(4, obj.getPorcentajePago());
            sqlComando.setInt(5, obj.getPlazo());
            sqlComando.executeUpdate();
        }
        catch(SQLException e){
            System.out.println("Error al insertar "+ e.getMessage());
        }
        desconectar();
    }
    
    public void actualizar(Cotizacion obj){
        conectar();
        try{
            sqlConsulta = "UPDATE cotizaciones set descripcion = ?, precio = ?, " 
                    + " porcentaje = ?, plazos=? WHERE numCotizacion = ?";
            System.out.println("sqlConsulta");
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            sqlComando.setString(1, obj.getDescripcionAutomovil());
            sqlComando.setInt(2, obj.getPrecioAutomovil());
            sqlComando.setInt(3, obj.getPorcentajePago());
            sqlComando.setInt(4, obj.getPlazo());
            sqlComando.setString(5, obj.getNumCotizacion());
            sqlComando.executeUpdate();
        }
        catch(SQLException e){
            System.out.println("Error al actualizar "+ e.getMessage());
        }
        desconectar();
    }
    
    public void borrar(Cotizacion obj){
        conectar();
        try{
            sqlConsulta = "DELETE FROM cotizaciones " + "WHERE numCotizacion = ?";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            sqlComando.setString(1, obj.getNumCotizacion());
            sqlComando.executeUpdate();
        }
        catch(SQLException e){
            System.out.println("Error al borrar "+ e.getMessage());
        }
        desconectar();
    }
    
    public Cotizacion consultar(Cotizacion obj){
        Cotizacion cotiza = new Cotizacion();
        conectar();
        try{
            sqlConsulta = "SELECT * from cotizaciones where numCotizacion = ?";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            sqlComando.setString(1, obj.getNumCotizacion());
            registros = sqlComando.executeQuery();
            if(registros.next()){
                cotiza.setIdCotizacion(registros.getInt("idCotizacion"));
                cotiza.setDescripcionAutomovil(registros.getString("descripcion"));
                cotiza.setPrecioAutomovil(registros.getInt("precio"));
                cotiza.setPorcentajePago(registros.getInt("porcentaje"));
                cotiza.setPlazo(registros.getInt("plazos"));
            }
            else{
                cotiza.setIdCotizacion(-1);
            }
        }
        catch(SQLException e){
            System.out.println("Error al consultar "+ e.getMessage());
        }
        desconectar();
        return cotiza;
    }
    
    public int numRegistros(){
        int numRegistros=0;
        conectar();
        try{
            sqlConsulta = "SELECT * FROM cotizaciones " ;
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            registros = sqlComando.executeQuery();
            while(registros.next())++numRegistros;
        }
        catch(SQLException e){
            System.out.println("Error al consultar " + e.getMessage());
        }
        return numRegistros;
    }
    
    public Object [][] cargarDatos(){
        int numRegistros = this.numRegistros();
        Object datos[][] = new Object[numRegistros][6];
        conectar();
        try{
            sqlConsulta = "SELECT * from cotizaciones order by idCotizacion";
            PreparedStatement sqlComando = conexion.prepareStatement(sqlConsulta);
            registros = sqlComando.executeQuery();
            int con = 0;
            while(registros.next()){
                datos[con][0] = registros.getInt("IdCotizacion");
                datos[con][1] = registros.getInt("numCotizacion");
                datos[con][2] = registros.getString("descripcion");
                datos[con][3] = registros.getInt("precio");
                datos[con][4] = registros.getInt("porcentaje");
                datos[con][5] = registros.getInt("plazos");
                ++con;
            }
        }
        catch(SQLException e){
            System.out.println("Error al consultar" + e.getMessage());
        }
        desconectar();
        return datos;
    }
    
    
    
    
    
}
